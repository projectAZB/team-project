package com.example.adam.yelpdemo;

import android.os.AsyncTask;

public class AsyncGetTask extends AsyncTask<String, Void, String> {

    //protocol method for returning the response back to the observer
    interface GetAsyncResponse {
        void getStringResponse(String response);
    }

    private GetAsyncResponse mGetAsyncResponse;

    //class initializer with async response callback
    public AsyncGetTask(GetAsyncResponse getAsyncResponse) {
        mGetAsyncResponse = getAsyncResponse;
    }

    //methods to override
    @Override
    protected String doInBackground(String... strings) {
        return APIClient.getInstance().getStringResponseFromUrlString(strings[0]);
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        mGetAsyncResponse.getStringResponse(response);
    }
}
