package com.example.adam.yelpdemo;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import java.io.Console;
import java.util.ArrayList;

public class BusinessAdapter extends RecyclerView.Adapter
{
    ArrayList<BusinessViewModel> mBusinessViewModels;


    public interface OnBusinessClickListener {
        void onBusinessClick(BusinessViewModel businessViewModel);
    }

    private OnBusinessClickListener mOnBusinessClickListener;

    public BusinessAdapter(ArrayList<BusinessViewModel> businesses, OnBusinessClickListener onBusinessClickListener) {
        mBusinessViewModels = businesses;
        mOnBusinessClickListener = onBusinessClickListener;
    }

    @NonNull
    @Override

    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new BusinessViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((BusinessViewHolder) holder).bind(mBusinessViewModels.get(position), mOnBusinessClickListener);
    }

    @Override
    public int getItemCount() {
        return mBusinessViewModels.size();
    }

    public static class BusinessViewHolder extends RecyclerView.ViewHolder {

        private ImageRequest mImageRequest;

        private TextView mDisplayNameTextView;
        private TextView mAddressTextView;
        private ImageView mListImageView;
        private TextView mRatingTextView;

        public BusinessViewHolder(View itemView) {
            super(itemView);
            mListImageView = itemView.findViewById(R.id.list_image_view);
            mDisplayNameTextView = itemView.findViewById(R.id.display_name_text_view);
            mAddressTextView = itemView.findViewById(R.id.address_text_view);
            mRatingTextView = itemView.findViewById(R.id.rating_text_view);
        }

        public void bind(final BusinessViewModel business, final OnBusinessClickListener onBusinessClickListener)
        {
            //cancel the image request for the dequeued cell
            if (mImageRequest != null && !mImageRequest.isCanceled()) {
                mImageRequest.cancel();
            }
            //create new image request
            mImageRequest = new ImageRequest(business.getImageUrl(), new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap response) {
                    mImageRequest = null;
                    mListImageView.setImageBitmap(response);
                }
            }, mListImageView.getWidth(), mListImageView.getHeight(), ImageView.ScaleType.FIT_CENTER, Bitmap.Config.RGB_565, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mImageRequest = null;
                }
            });
            //add tag for batch cancelling
            mImageRequest.setTag(RequestQueueManager.IMAGE_REQ_TAG);
            RequestQueueManager.getInstance(itemView.getContext()).addToRequestQueue(mImageRequest);
            mDisplayNameTextView.setText(business.displayName());
            mAddressTextView.setText(business.fullAddress());
            mRatingTextView.setText(business.rating());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBusinessClickListener.onBusinessClick(business);
                }
            });
        }

    }
}
