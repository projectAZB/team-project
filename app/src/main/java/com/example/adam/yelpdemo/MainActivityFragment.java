package com.example.adam.yelpdemo;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class MainActivityFragment extends Fragment
{
    private static final String TAG = "MainActivityFragment";

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    private ArrayList<BusinessViewModel> mBusinesses = new ArrayList<>();

    public MainActivityFragment() {
    }

    public static MainActivityFragment newInstance() {
        MainActivityFragment fragment = new MainActivityFragment();
        return fragment;
    }

    private void getBusinesses() {
        mBusinesses.clear();
        APIClient.getInstance().businessesForSearchTerm("cafe", getContext(), new APIClient.GetBusinessesAsyncCallback() {
            @Override
            public void onBusinessesLoaded(ArrayList<BusinessViewModel> businesses) {
                mBusinesses.addAll(businesses);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        mRecyclerView = v.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new BusinessAdapter(mBusinesses, new BusinessAdapter.OnBusinessClickListener() {
            @Override
            public void onBusinessClick(BusinessViewModel businessViewModel) {

            }
        });
        mRecyclerView.setAdapter(mAdapter);

        getBusinesses();

        return v;
    }


    @Override
    public void onStop() {
        super.onStop();
        RequestQueueManager.getInstance(getContext()).getRequestQueue().cancelAll(RequestQueueManager.IMAGE_REQ_TAG);
    }
}
