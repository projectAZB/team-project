package com.example.adam.yelpdemo;

public class Business {
    private String mBusinessID;
    private String mAlias;
    private String mDisplayName;
    private String mImageUrl;
    private String mUrl;
    private Integer mRating;
    private String mAddress;
    private String mCity;
    private String mState;
    private String mCountry;

    public Business(String businessID, String alias,
                    String displayName, String imageUrl,
                    String url, Integer rating,
                    String address, String city,
                    String state, String country) {
        mBusinessID = businessID;
        mAlias = alias;
        mDisplayName = displayName;
        mImageUrl = imageUrl;
        mUrl = url;
        mRating = rating;
        mAddress = address;
        mCity = city;
        mState = state;
        mCountry = country;
    }


    public String getBusinessID() {
        return mBusinessID;
    }

    public void setBusinessID(String businessID) {
        mBusinessID = businessID;
    }

    public String getAlias() {
        return mAlias;
    }

    public void setAlias(String alias) {
        mAlias = alias;
    }

    public String getDisplayName() {
        return mDisplayName;
    }

    public void setDisplayName(String displayName) {
        mDisplayName = displayName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public Integer getRating() {
        return mRating;
    }

    public void setRating(Integer rating) {
        mRating = rating;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public void printBusiness() {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Display : " + getDisplayName());
        System.out.println("Alias : " + getAlias());
        System.out.println("Address : " + getAddress());
        System.out.println("Business : " + getBusinessID());
        System.out.println("City : " + getCity());
        System.out.println("State : " + getState());
        System.out.println("Country : " + getCountry());
        System.out.println("Image : " + getImageUrl());
        System.out.println("Url : " + getUrl());
        System.out.println("Rating : " + getRating());
    }
}
