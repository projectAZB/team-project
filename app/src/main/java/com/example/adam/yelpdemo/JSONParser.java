package com.example.adam.yelpdemo;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONParser {

    private static final String TAG = "JSONParser";

    public static ArrayList<BusinessViewModel> businessesFromJSONString(String jsonBodyString) throws JSONException
    {
        ArrayList<BusinessViewModel> businesses = new ArrayList<>();
        JSONObject jsonBody = new JSONObject(jsonBodyString);
        JSONArray businessesArray = jsonBody.getJSONArray("businesses");
        for (int i = 0; i < businessesArray.length(); i++)
        {
            JSONObject businessObject = businessesArray.getJSONObject(i);
            //Log.d(TAG, businessObject.toString());
            String businessID = businessObject.getString("id");
            String alias = businessObject.getString("alias");
            String displayName = businessObject.getString("name");
            String imageUrl = businessObject.getString("image_url");
            String url = businessObject.getString("url");
            Integer rating = businessObject.getInt("rating");
            JSONObject locationObject = businessObject.getJSONObject("location");
            String address = locationObject.getString("address1");
            String city = locationObject.getString("city");
            String state = locationObject.getString("state");
            String country = locationObject.getString("country");
            Business business = new Business(businessID, alias, displayName, imageUrl, url, rating, address, city, state, country);
            businesses.add(new BusinessViewModel(business));
            //business.printBusiness();
        }
        return businesses;
    }
}
