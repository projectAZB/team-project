package com.example.adam.yelpdemo;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class APIClient
{
    // interfaces
    public interface GetBusinessesAsyncCallback
    {
        void onBusinessesLoaded(ArrayList<BusinessViewModel> businesses);
    }

    private static final String TAG = "APIClient";

    private static APIClient sAPIClient;

    private APIClient() {
    }

    public static APIClient getInstance() {
        if (sAPIClient == null) {
            sAPIClient = new APIClient();
        }
        return sAPIClient;
    }

    private static byte[] getUrlBytesFromUrlString(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestProperty("Authorization", "Bearer -j5q_p8-XeFG22Pw2PuKUTmPwVg3-69FavYjckloEjygeV5e79k4_0Ny0OvNGTX2u3XWZOQTGoIdwfsp1L1nR4-rB0GeUX6H3nUaPD8q2-U1BYy_Be4YuOAiyQQFW3Yx");

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = httpURLConnection.getInputStream();
            if (httpURLConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(httpURLConnection.getResponseMessage() + urlString);
            }
            int bytesRead;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        }
        finally {
            httpURLConnection.disconnect();
        }
    }

    public String getStringResponseFromUrlString(String urlString)
    {
        try {
            byte[] urlBytes = APIClient.getUrlBytesFromUrlString(urlString);
            return new String(urlBytes);
        }
        catch (IOException e) {
            Log.e("IOException", "Error getting: " + urlString);
        }
        return "";
    }

    public void businessesForSearchTerm(String term, Context context, final GetBusinessesAsyncCallback getBusinessesAsyncCallback) {
        String urlString = context.getResources().getString(R.string.yelp_businesses_api, term);
        AsyncGetTask asyncGetTask = new AsyncGetTask(new AsyncGetTask.GetAsyncResponse() {
            @Override
            public void getStringResponse(String response) {
                ArrayList<BusinessViewModel> businesses = new ArrayList<>();
               try {
                   businesses = JSONParser.businessesFromJSONString(response);
                   getBusinessesAsyncCallback.onBusinessesLoaded(businesses);
               }
               catch (JSONException jsone) {
                   Log.e("JSON Error", jsone.getLocalizedMessage());
                   getBusinessesAsyncCallback.onBusinessesLoaded(businesses);
               }
            }
        });
        asyncGetTask.execute(urlString);
    }
}
