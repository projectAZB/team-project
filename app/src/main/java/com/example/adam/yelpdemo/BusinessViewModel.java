package com.example.adam.yelpdemo;

public class BusinessViewModel
{
    private Business mBusiness;

    public BusinessViewModel(Business business) {
        mBusiness = business;
    }

    public String displayName() {
        return mBusiness.getDisplayName();
    }

    public String getImageUrl() {
        return mBusiness.getImageUrl();
    }

    public String rating() {
        return mBusiness.getRating() + " stars";
    }

    public String fullAddress() {
        if (!mBusiness.getState().isEmpty()) {
            return mBusiness.getAddress() + ", " + mBusiness.getCity() + ", " + mBusiness.getState();
        }
        else {
            return mBusiness.getAddress() + ", " + mBusiness.getCity() + ", " + mBusiness.getCountry();
        }
    }
}
